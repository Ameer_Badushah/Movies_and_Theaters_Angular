import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MovieAddComponent } from './components/movie-add/movie-add.component';
import { TheaterAddComponent } from './components/theater-add/theater-add.component';
import { AboutComponent } from './components/about/about.component';
import { MovieDetailedComponent } from './components/movie-detailed/movie-detailed.component';
import { EditComponent } from './components/edit/edit.component';

const routes: Routes = [
{
  path:'',
  component:HomeComponent
},{
  path:'addMovie',
  component:MovieAddComponent
},
{
  path:'addTheater',
  component:TheaterAddComponent
},{
  path:'about',
  component:AboutComponent
},{
  path:'movieDetails/:id',
  component:MovieDetailedComponent
},{
  path:'edit/:id',
  component:EditComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
