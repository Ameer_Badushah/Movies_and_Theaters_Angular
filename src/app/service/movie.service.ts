import { HttpClient } from '@angular/common/http';
import { Injectable, WritableSignal, signal } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Movies } from '../components/movie-add/movie-add.component';
import { Theaters } from '../components/theater-add/theater-add.component';

@Injectable({
  providedIn: 'root',
})
export class MovieService {


  private moviesUrl = 'http://localhost:3000/movies';
  private theaterUrl = 'http://localhost:3000/theater'

  //signal created
  public movieData = signal<Movies[]>([]);
  public theaterData = signal<Theaters[]>([]);

  constructor(private http: HttpClient) {}

  setMovies(movie: Movies) {
    this.movieData.mutate((val) => {
      val.push(movie);
    });
  }

  setTheater(theater: Theaters) {
    this.theaterData.mutate((val) => {
      val.push(theater);
      console.log("here set Theater service")
    });
  }

  getMovies(): Observable<any> {
    return this.http.get<any>(this.moviesUrl);
  }

  getTheater():Observable<any>{
    return this.http.get<any>(this.theaterUrl);

  }
  getMovieDetails(movieId: number): Observable<Movies> {
    return this.http.get<Movies[]>(this.moviesUrl).pipe(
      map((movies) => movies.find((movie) => movie.id === movieId))
    );
  }


  // theater details in movie Details

  getTheatersForMovie(movieTitle: string): Observable<Theaters[]> {
    return this.http.get<Theaters[]>(this.theaterUrl).pipe(
      map(theaters => theaters.filter(theater => theater.movies.includes(movieTitle)))
    );
  }


  updateMovie(id: number, value: any): Observable<any> {
    const moviesUrl = `${this.moviesUrl}/${id}`;
    return this.http.put(moviesUrl, value);
  }

}
