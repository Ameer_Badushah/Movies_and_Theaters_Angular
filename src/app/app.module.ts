import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TheaterAddComponent } from './components/theater-add/theater-add.component';
import { MovieAddComponent } from './components/movie-add/movie-add.component';
import { MenuComponent } from './components/menu/menu.component';
import { MovieCardComponent } from './components/movie-card/movie-card.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TableShowComponent } from './components/table-show/table-show.component';
import { MovieDetailedComponent } from './components/movie-detailed/movie-detailed.component';
import { EditComponent } from './components/edit/edit.component';
import { MovieEditComponent } from './components/edit/movie-edit/movie-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    TheaterAddComponent,
    MovieAddComponent,
    MenuComponent,
    MovieCardComponent,
    HomeComponent,
    AboutComponent,
    TableShowComponent,
    MovieDetailedComponent,
    EditComponent,
    MovieEditComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
